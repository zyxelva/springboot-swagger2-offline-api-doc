# swagger2-offline-api-doc

You can open the file offline-api-doc.html in browser to see example.

![preview](https://upload-images.jianshu.io/upload_images/7882361-241200054f7edd90.png?imageMogr2/auto-orient/strip%7CimageView2/2/w/1240)

dev-guide：https://www.jianshu.com/p/033d650164c4


https://github.com/Swagger2Markup/spring-swagger2markup-demo
https://github.com/Swagger2Markup/swagger2markup
https://swagger2markup.github.io/swagger2markup/1.3.3/#_spring_boot_and_springfox
https://springfox.github.io/springfox/docs/current/#configuring-springfox-staticdocs

PDF中文乱码问题，参考[swagger+asciidoctor 导出PDF中文缺失乱码问题解决](https://blog.csdn.net/qq_29534483/article/details/81235081)