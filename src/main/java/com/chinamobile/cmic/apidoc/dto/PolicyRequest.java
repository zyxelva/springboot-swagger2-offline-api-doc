package com.chinamobile.cmic.apidoc.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * @author zyxelva
 */
@ApiModel(description = "保单返回实体")
@Data
@ToString
public class PolicyRequest implements Serializable {
    private static final long serialVersionUID = 1L;
    @ApiModelProperty(value = "用户id")
    private String userId;
    @ApiModelProperty(value = "人工审核完成之前图片的名称")
    private String name;
    @ApiModelProperty(value = "保险单号(手工录入)")
    private String policyNumber;
    @ApiModelProperty(value = "保险合同编号")
    private String contractNumber;
    @ApiModelProperty(value = "保险公司编号")
    private String companyNumber;
    @ApiModelProperty(value = "产品名称")
    private String majorProductName;
    @ApiModelProperty(value = "产品编号")
    private String majorProductId;
    @ApiModelProperty(value = "主险金额")
    private BigDecimal amount;
    @ApiModelProperty(value = "币种")
    private String currency;
    @ApiModelProperty(value = "合同生效日期")
    private Date effectiveDate;
    @ApiModelProperty(value = "主险期满日期")
    private Date expireDate;
    @ApiModelProperty(value = "主险期交保险费")
    private BigDecimal payAmount;
    @ApiModelProperty(value = "主险缴费周期（单位：月）12-年交，6-半年交，3-季交，1-月交，0-趸交，13 -交至几岁，14 -交至终身")
    private Short paymentMode;
    @ApiModelProperty(value = "主险缴费期间(以月为单位)")
    private Short paymentTerm;
    @ApiModelProperty(value = "主险、附加险累计期交保费")
    private BigDecimal summationPremium;
    @ApiModelProperty(value = "用户上传的保单图片链接")
    private String url;
    @ApiModelProperty(value = "保单状态：1. 成功上传；2. 已取消识别；3. 人工识别中；4. 识别失败；5. 待生效；6. 保障中；7. 保单终止；8 已退保；9 已作废")
    private Short status;
    @ApiModelProperty(value = "展示类型：1-标准型，2-非标准型")
    private Integer showType;
    @ApiModelProperty(value = "产品类别")
    private Integer productType;
    @ApiModelProperty(value = "可配置型购买问题")
    private String buyQuestion;
    @ApiModelProperty(value = "可配置型购买答案")
    private String buyAnswer;
    @ApiModelProperty(value = "1. 传统型，电话理赔【保单管家】，拨打【理赔电话】2. 传统型，线上理赔，线上提交理赔申请资料3. 创新型，抵扣型补偿，补偿申请工单，补偿方式：在线支付立减XXXX元4. 创新型，返现型补偿，补偿申请工单，补偿方式：现金型蜗牛币转入")
    private Integer compensationType;
    @ApiModelProperty(value = "是否新数据：0-旧数据，1-新数据判断新旧的依据：安卓和iOS从v2.0.1(包含)开始，为新数据")
    private Boolean newFlag;
    @ApiModelProperty(value = "保单处理结果")
    private String processResult;
    @ApiModelProperty(value = "记录创建者")
    private String createBy;
    @ApiModelProperty(value = "记录创建时间")
    private Date createTime;
    @ApiModelProperty(value = "记录更新者")
    private String updateBy;
    @ApiModelProperty(value = "记录更新时间")
    private Date updateTime;
    @ApiModelProperty(value = "对应病历添加页面中,对现有保单的处理操作,0=>退保,1=>保留,2=>延迟退保")
    private Integer hold;
    @ApiModelProperty(value = "对应病历添加页面中,对现有保单的处理操作说明")
    private String explanation;
    @ApiModelProperty(value = "app上传还是H5上传的标志   0:app  1:H5 2wap")
    private Integer uploadFlag;
    @ApiModelProperty(value = "是否完成并在界面显示0：完成1：未完成不显示")
    private Integer done;
    @ApiModelProperty(value = "承保重试次数，默认null；lians承保失败后，该值设置3")
    private Integer retry;
    @ApiModelProperty(value = "关联insProduct表的ID，用于保单诊断")
    private String insproductId;
    @ApiModelProperty(value = "保单备注信息.华贵擎天柱中有用到.和谐理财险备注")
    private String remark;
    @ApiModelProperty(value = "保至多少岁/保多少年类型.华贵的定义:A=>保N年，B=>保至N岁，C=>保终身.etc B70 保至70岁")
    private String insuredTime;
    @ApiModelProperty(value = "续费续保跟踪备注")
    private String renewalRemark;
    @ApiModelProperty(value = "0--人身保险   1--车险  2理财")
    private Integer type;
    @ApiModelProperty(value = "关联user_car表")
    private String userCarId;
    @ApiModelProperty(value = "保险公司名称")
    private String companyName;
    @ApiModelProperty(value = "承保险种，多个逗号分割   1--交通强制险   2--车辆损失险  3--第三者责任险  4--全车盗抢险5--玻璃单独破碎险  6--自燃损失险7--车上人员责任险（驾驶员）  8--车上人员责任险（乘客）  9--不计免赔险  10--车辆涉水险  11--车身划痕险")
    private String carInsureType;
    @ApiModelProperty(value = "针对图片保单，是否在前端显示   0--显示   1--隐藏")
    private Integer hide;
    @ApiModelProperty(value = "续保的新保单号")
    private String renewPolicyId;
    @ApiModelProperty(value = "保单份数")
    private Integer count;
    @ApiModelProperty(value = "如果paymentMode=13或14就录入 paymentMode=13，paymentTermAge录入被保人岁数数字paymentMode=14，paymentTermAge录入数字150")
    private String paymentTermAge;
    @ApiModelProperty(value = "期满日期类型1 日期2 岁数3 保至终身（1）后台数据为日期，前端显示日期（2）后台数据为岁数，前端显示X岁（3）后台数据为保至终身，前端显示保至终身")
    private Integer expireDateType;
    @ApiModelProperty(value = "如果expireDateType=2或3就录入 expireDateAge=2，录入被保人岁数数字expireDateAge=3，录入数字150")
    private String expireDateAge;
    @ApiModelProperty(value = "退保时间")
    private Date retreatsTime;
    @ApiModelProperty(value = "数据生成类型：0:系统生成，1:系统补录")
    private Short recordType;
    @ApiModelProperty(value = "保单来源：0:线下保单，1:线上保单")
    private Short policySource;
    @ApiModelProperty(value = "是否自动续保单：0:否，1:是")
    private Short autoRenewal;
    @ApiModelProperty(value = "费率计划")
    private String planType;
}